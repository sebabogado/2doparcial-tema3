<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tema 3</title>
</head>
<body>
<?php

// Definir constante para la cantidad de filas y columnas
define('TAMANO_MATRIZ', 3);

// Función para generar una matriz cuadrada con números aleatorios entre 0 y 9
function generarMatrizAleatoria($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        for ($j = 0; $j < $tamano; $j++) {
            $matriz[$i][$j] = rand(0, 9);
        }
    }
    return $matriz;
}

// Función para imprimir la matriz y calcular la suma de la diagonal principal
function imprimirMatrizYSumaDiagonal($matriz) {
    $sumaDiagonal = 0;
    echo "<table border='1'>";
    for ($i = 0; $i < count($matriz); $i++) {
        echo "<tr>";
        for ($j = 0; $j < count($matriz[$i]); $j++) {
            echo "<td>" . $matriz[$i][$j] . "</td>";
            if ($i == $j) {
                $sumaDiagonal += $matriz[$i][$j];
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    echo "<br>";
    return $sumaDiagonal;
}

// Bucle principal
do {
    $matriz = generarMatrizAleatoria(TAMANO_MATRIZ);
    $sumaDiagonal = imprimirMatrizYSumaDiagonal($matriz);

    if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
        echo "La suma de la diagonal es $sumaDiagonal. El script termina.";
        break;
    }
} while (true);

?>
</body>
</html>